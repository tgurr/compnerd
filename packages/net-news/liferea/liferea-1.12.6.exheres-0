# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require freedesktop-desktop gsettings gtk-icon-cache
# TODO pnv can be removed in next release.
# This weird pnv-b just to avoid patching issue https://github.com/lwindolf/liferea/issues/694
require github [ user=lwindolf suffix=tar.bz2 release=v${PV} pnv=${PNV}b ]
require python [ blacklist=2 multibuild=false has_lib=false ]

SUMMARY="Linux Feed Reader"
HOMEPAGE="http://lzone.de/liferea/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection [[ description = [ Plugins support ] ]]
    libnotify [[ requires = gobject-introspection ]]
    ( linguas: ar ast be@latin bg ca cs da de el es eu fi fr gl he hu id it ja ko lt lv mk nl pl pt
               pt_BR ro ru sk sq sv tr uk vi zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.4]
        virtual/pkg-config[>=0.20]
    build+run:
        core/json-glib
        dev-db/sqlite:3[>=3.7]
        dev-libs/glib:2[>=2.28]
        dev-libs/libpeas:1.0[>=1.0.0]
        dev-libs/libxml2:2.0[>=2.6.27]
        dev-libs/libxslt[>=1.1.19]
        gnome-desktop/gsettings-desktop-schemas
        gnome-desktop/libsoup:2.4[>=2.42]
        net-libs/webkit:4.0
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.14.0][gobject-introspection?]
        x11-libs/pango[>=1.4.0]
        gobject-introspection? (
            gnome-desktop/gobject-introspection:1[>=0.9.3]
            gnome-bindings/pygobject:3
        )
        libnotify? ( x11-libs/libnotify[>=0.7] )
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
)

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

